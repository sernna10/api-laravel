<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Rutas para vistas
Route::view('/', 'welcome')->name('welcome');
Route::view('/clientes', 'clientes')->name('clientes');
Route::view('/login', 'login')->name('login');

// Rutas CRUD
Route::post('/api/register', 'UserController@register');
Route::post('/api/login', 'UserController@login');

Route::resource('/api/clientes', 'ClienteController');