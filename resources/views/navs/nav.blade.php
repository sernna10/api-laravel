<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand"  href="{{ route('welcome') }}">KONECTA</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
        aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('welcome') }}">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('clientes') }}">Usuarios</a>
            </li>
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">Login</a>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">Cerrar sesión</a>
                </li>
            @endguest

        </ul>
        <span class="navbar-text">
            <form id="logout-form" action="" method="POST" class="d-none">
                @csrf
            </form>
        </span>
    </div>
</nav>
