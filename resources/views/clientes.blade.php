@extends('layout')

@section('titulo', 'Clientes')

@section('content')
    <div class="container">
        <div class="section">
            <button class="btn btn-primary m-3" id="boton" onclick="traer()">Listar tabla</button>

            <table class="table table-hover table-dark">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Documento</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Correo</th>
                    </tr>
                </thead>
                <tbody id="respuesta">
                    
                </tbody>
            </table>

        </div>
    </div>
@endsection
