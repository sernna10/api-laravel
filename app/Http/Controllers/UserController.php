<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Libreria JWT
use App\Helpers\JwtAuth;

// Libreria de la Base de Datos
use Illuminate\Support\Facades\DB;

// Modelo Usuario
use App\User;

class UserController extends Controller
{
    public function register(Request $request){
        // Recoger post
        $json = $request->input('json', null);
        $params = json_decode($json);

        $email = ( !is_null($json) && isset($params->email) ) ? $params->email : null;
        $name = ( !is_null($json) && isset($params->name) ) ? $params->name : null;
        $surname = ( !is_null($json) && isset($params->surname) ) ? $params->surname : null;
        $role = ( !is_null($json) && isset($params->role) ) ? $params->role : null;
        $password = ( !is_null($json) && isset($params->password) ) ? $params->password : null;

        if (!is_null($email) && !is_null($password) && !is_null($name) && !is_null($surname) && !is_null($role)) {
            
            
            // Crear Usuario
            $user = new User();
            
            $user->email = $email;
            $user->name = $name;
            $user->surname = $surname;
            $user->role = $role;

            // Encriptar contraseña
            $pwd = hash('sha256', $password);
            $user->password = $pwd;

            // Comprobar usuario duplicado
            $isset_user = (array)User::where('role', '=', $role)->first();

            if (count($isset_user) == 0) {
                
                // Guardar usuario
                $user->save();

                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Usuario registrado correctamente.'
                );

            } else {

                // No guardar usuario, ya existe
                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario con mismo Role existente, no puede registrarse.'
                );

            }
            

        }else{
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no creado.'
            );
        }

        return response()->json($data, 200);

    }
    
    public function login(Request $request){
        
        $jwtAuth = new JwtAuth();

        // Recibir POST
        $json = $request->input('json', null);
        $params = json_decode($json);

        $email = (!is_null($json) && isset($params->email)) ? $params->email : null;
        $password = (!is_null($json) && isset($params->password)) ? $params->password : null;
        $getToken = (!is_null($json) && isset($params->gettoken)) ? $params->gettoken : true;

        // Encriptar password
        $pwd = hash('sha256', $password);

        if (!is_null($email) && !is_null($password) && ($getToken == null || $getToken == 'false')) {
            $signup = $jwtAuth->singup($email, $pwd); //Utilizamos el metodo creado en el archivo JwtAuth.php
        }elseif($getToken != null){
            $signup = $jwtAuth->singup($email, $pwd, $getToken);
        }else{
            $signup = array(
                'status' => 'error',
                'message' => 'Envía tus datos por POST.'
            );
        }

        return response()->json($signup, 200);

    }
    
}
