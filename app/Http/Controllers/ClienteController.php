<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Libreria JWT
use App\Helpers\JwtAuth;

// Modelo Cliente
use App\Cliente;

class ClienteController extends Controller
{
    public function index(){
        
        // Obtener listado de todos los clientes
        $clientes = Cliente::all()->load('user');

        return response()->json(array(
            'clientes' => $clientes,
            'status' => 'success'
        ), 200);
        
    }

    public function show($id){
        $cliente = Cliente::find($id)->load('user');

        return response()->json(array(
            'cliente' => $cliente,
            'status' => 'success'
        ), 200);
    }

    public function update($id, Request $request){

        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash); //Utilizo el metodo creado en el archivo JwtAuth.php

        if ($checkToken) {

            // Recoger datos por POST
            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json, true);

            // Validación
            $validate = \Validator::make($params_array, [
                'documento' => 'required',
                'name' => 'required',
                'correo' => 'required'
            ]);

            if ($validate->fails()) {
                return response()->json($validate->errors(), 400);
            }

            // Actualizar Cliente
            $cliente = Cliente::where('id', $id)->update($params_array);

            $data = array(
                'cliente' => $params,
                'status' => 'success',
                'code' => 200
            );

        }else{
            // Devolver error
            $data = array(
                'message' => 'Login incorrecto.',
                'status' => 'error',
                'code' => 400
            );

        }

        return response()->json($data, 200);
        
    }

    public function store(Request  $request){

        $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash); //Utilizo el metodo creado en el archivo JwtAuth.php

        if ($checkToken) {

            // Recoger datos por POST
            $json = $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json, true);
            
            // Conseguir el usuario identificado
            $user = $jwtAuth->checkToken($hash, true);

            // Validación
            $validate = \Validator::make($params_array, [
                'documento' => 'required',
                'name' => 'required',
                'correo' => 'required',
                'role' => 'required'
            ]);

            if ($validate->fails()) {
                return response()->json($validate->errors(), 400);
            }

            // Guardar Cliente
            $cliente = new Cliente();

            $cliente->documento = $params->documento;
            $cliente->name = $params->name;
            $cliente->correo = $params->correo;
            $cliente->idUser = $user->sub;
            $cliente->save();

            $data = array(
                'Cliente' => $cliente,
                'status' => 'success',
                'code' => 200
            );

        }else{
            // Devolver error
            $data = array(
                'message' => 'Login incorrecto.',
                'status' => 'error',
                'code' => 400
            );

        }

        return response()->json($data, 200);

    }

    public function destroy($id, Request $request){

        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();

        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            // Comprobar que existe el registro
            $cliente = Cliente::find($id);

            // Borrarlo
            $cliente->delete();

            // Devolver registro borrado
            $data = array(
                'cliente' => $cliente,
                'status' => 'success',
                'code' => 200
            );

        }else{
            $data = array(
                'status' => 'error',
                'message' => 'Login incorrecto.',
                'code' => 400
            );
        }

        return response()->json($data, 200);

    }

}
