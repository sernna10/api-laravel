<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'tblClientes';

    // Relación
    public function user(){

        // Otro modelo a relacionar, propiedad para relacionar modelos
        return $this->belongsTo('App\User', 'idUser');

    }
}
