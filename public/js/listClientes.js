var resp = document.querySelector('#respuesta');

function traer() {
    fetch('api/clientes')
        .then(res => res.json())
        .then(datos => {
            tabla(datos)
        })
}

function tabla(datos) {

    console.log(datos)
    
    resp.innerHTML = '';

    for (let item of datos) {

        resp.innerHTML += `
            <tr>
                <th scope="row">${item.id}</th>
                <td>${item.documento}</td>
                <td>${item.name}</td>
                <td>${item.correo}</td>
            </tr>
        `

    }

}